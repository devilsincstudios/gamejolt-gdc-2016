// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core.Behaviours;
using UnityEngine;

namespace DI.AI
{
	public class AIWaypoint : DI_MonoBehaviour
	{
		public Color pathColor = Color.green;
		public AIWaypoint nextWaypoint;
		public float timeToReachWaypoint;

		public void OnDrawGizmos()
		{
			if (nextWaypoint != null) {
				Gizmos.color = pathColor;
				Gizmos.DrawLine(transform.position, nextWaypoint.transform.position);
			}
		}
		public void OnDrawGizmosSelected()
		{
			if (nextWaypoint != null) {
				Gizmos.DrawCube(nextWaypoint.transform.position, new Vector3(1f, 1f, 1f));
			}
		}
	}
}