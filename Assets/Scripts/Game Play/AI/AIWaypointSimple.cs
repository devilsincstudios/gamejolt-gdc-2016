// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core.Behaviours;
using DI.Core;
using UnityEngine;
using System.Collections.Generic;

namespace DI.AI
{
	public class AIWaypointSimple : DI_MonoBehaviour
	{
		public AIWaypoint waypoint;

		public Rigidbody2D rigidBody;
		public bool isMoving = false;
		public Vector3 startPosition;
		public Vector3 endPosition;

		private float startTime;

		public void Update()
		{
			if (!isMoving) {
				if (waypoint != null) {
					isMoving = true;
					startTime = DI_Time.getTimeSinceLevelLoad();
					startPosition = transform.position;

					// Ignore the waypoints z position.
					endPosition = new Vector3(waypoint.transform.position.x, waypoint.transform.position.y, transform.position.z);
				}
				else {
					// We have run out of waypoints
					this.enabled = false;
				}
			}
		}

		public void FixedUpdate()
		{
			if (isMoving) {
				float timeSinceStarted = DI_Time.getTimeSinceLevelLoad() - startTime;
				float percentageComplete = timeSinceStarted / waypoint.timeToReachWaypoint;
				Vector3 newPosition = Vector3.Lerp (startPosition, endPosition, percentageComplete);
				rigidBody.MovePosition(new Vector2(newPosition.x, newPosition.y));

				if(percentageComplete >= 1.0f)
				{
					isMoving = false;
					waypoint = waypoint.nextWaypoint;
				}
			}
		}

		public void OnDrawGizmos()
		{
			if (waypoint != null) {
				Gizmos.color = waypoint.pathColor;
				Gizmos.DrawLine(transform.position, waypoint.transform.position);
			}
		}
		public void OnDrawGizmosSelected()
		{
			if (waypoint != null) {
				Gizmos.DrawCube(waypoint.transform.position, new Vector3(1f, 1f, 1f));
			}
		}
	}
}