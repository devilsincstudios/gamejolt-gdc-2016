/*
*
* 	Devils Inc Studios
* 	// Copyright DEVILS INC. STUDIOS LIMITED 2014, 2015, 2015
*	
*	Disables the mesh renderer for editor only objects once the game starts.
*	This allows them to be seen while in editor mode, but not while in game.
*
*/

using UnityEngine;
using System.Collections;
using System;

using DI.Core.Behaviours;

namespace DI.Tools
{
	[AddComponentMenu("Devil's Inc Studios/Tools/Editor Only Object")]
	public class EditorOnlyObject : DI_MonoBehaviour
	{
		public void Start() {
			// Get the mesh renderer and disable it.
			try {
				this.gameObject.GetComponent<MeshRenderer>().enabled = false;
			}
			catch (Exception) {
			}

			try {
				this.gameObject.GetComponent<SpriteRenderer>().enabled = false;
			}
			catch (Exception) {
			}

			try {
				this.gameObject.GetComponent<SkinnedMeshRenderer>().enabled = false;
			}
			catch (Exception) {
			}
		}
	}
}
