// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;

using DI.Core.Behaviours;
using DI.Core.Events;

namespace DI.Camera
{
	public class CellCamera : DI_MonoBehaviour
	{
		[Header("Camera Settings")]
		[HeaderAttribute("Camera Bounds, (Min,Max)")]
		public Vector2 cameraXBounds;
		public Vector2 cameraYBounds;
		[HeaderAttribute("Camera Offset, Add this to the camera position.")]
		public Vector3 cameraOffset;
		[Header("Target Settings")]
		public GameObject player;
		public GameObject gravitySource;

		private bool isInGravity = false;
		private Vector3 newPosition;

		public void OnEnable()
		{
			DI_EventCenter<GameObject, GameObject, float, float>.addListener("OnGravityEnter", handleOnGravityEnter);
			DI_EventCenter<GameObject>.addListener("OnGravityExit", handleOnGravityExit);
			newPosition = Vector3.zero;
		}
		
		public void OnDisable()
		{
			DI_EventCenter<GameObject, GameObject, float, float>.removeListener("OnGravityEnter", handleOnGravityEnter);
			DI_EventCenter<GameObject>.removeListener("OnGravityExit", handleOnGravityExit);
		}
		
		public void handleOnGravityEnter(GameObject objectEntering, GameObject objectSupplying, float rotationSpeed, float jumpSpeed)
		{
			if (objectEntering == player) {
				isInGravity = true;
				gravitySource = objectSupplying;
			}
		}

		public void handleOnGravityExit(GameObject objectExiting)
		{
			if (objectExiting == player) {
				isInGravity = false;
				gravitySource = null;
			}
		}

		public void Update()
		{

			if (!isInGravity) {
				newPosition = player.transform.position + cameraOffset;
			}
			else {
				newPosition = gravitySource.transform.position + cameraOffset;
			}

			// Check to see if the camera is still in bounds.
			if (newPosition.x < cameraXBounds.x) {
				newPosition = new Vector3(cameraXBounds.x, newPosition.y, newPosition.z);
			}
			if (newPosition.x > cameraXBounds.y) {
				newPosition = new Vector3(cameraXBounds.y, newPosition.y, newPosition.z);
			}
			if (newPosition.y < cameraYBounds.x) {
				newPosition = new Vector3(newPosition.x, cameraYBounds.x, newPosition.z);
			}
			if (newPosition.y > cameraYBounds.y) {
				newPosition = new Vector3(newPosition.x, cameraYBounds.y, newPosition.z);
			}

			transform.position = newPosition;
		}
	}
}