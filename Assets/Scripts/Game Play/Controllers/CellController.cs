// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core.Behaviours;
using DI.Core.Input;
using DI.Core.Events;
using DI.Mechanics.Gravity;
using DI.Core;
using DI.Core.Helpers;
using DI.SFX;
using DI.UI;
using DI.Core.StateMachine;
using DI.Core.Debug;

using UnityEngine.UI;
using UnityEngine;
using System.Collections.Generic;
using System;

namespace DI.Controllers
{
	public class CellController : StateMachine
	{
		private Vector3 mouseCords;

		[Header("Player Settings.")]
		public int playerId = 0;
		public Gravity2D gravity;
		public Transform spawnPoint;
		public bool isPlaying = false;

		[Header("Energy Settings.")]
		public float maxEnergy = 100.0f;
		public float currentEnergy = 100.0f;
		public float energyDrainRate = 1.0f;
		public float energyRegenRate = 0.25f;
		public Image energyMeter;
		public Text energyMeterText;

		[Header("Boost Settings.")]
		public bool isAiming = false;
		public Vector2 boostForce;
		public GameObject pointer;
		private float boostCooldownTimer = 0.0f;
		public float boostEnergyCost = 25.0f;
		public float boostCooldown = 0.2f;
		public Vector3 boostDirection;

		[Header("Slow Time Settings.")]
		public bool isSlowingTime = false;
		public float slowEffect = 0.25f;

		private int jumpKeyIndex;
		//private int powerKeyIndex;

		[Header("SFX Settings.")]
		public List<DI_SFXClip> impactSFX;
		public List<DI_SFXClip> deathSFX;
		public List<DI_SFXClip> abilitiySFX;

		private Vector3 lastPosition = Vector3.zero;

		#region Setup
		protected void Awake() 
		{
			InitializeStateMachine<PlayerState>(PlayerState.Idle, true);
			
			#region Transistions Idling
			AddTransitionsToState(
				PlayerState.Idle,
				new Enum[] {
					PlayerState.Orbiting,
					PlayerState.Flying
				}
			);
			#endregion
			
			#region Transistions Orbiting
			AddTransitionsToState(
				PlayerState.Orbiting,
				new Enum[] {
					PlayerState.Idle,
					PlayerState.Flying,
					PlayerState.Aiming
				}
			);
			#endregion
			
			#region Transistions Flying
			AddTransitionsToState(
				PlayerState.Flying,
				new Enum[] {
					PlayerState.Idle,
					PlayerState.Orbiting
				}
			);
			#endregion

			#region Transistions Aiming
			AddTransitionsToState(
				PlayerState.Aiming,
				new Enum[] {
					PlayerState.Flying
				}
			);
			#endregion
		}
		#endregion

		#region Event Handling
		public void OnEnable()
		{
			updateKeyCache();
			DI_EventCenter<FadeType, string>.addListener("OnFadeEnd", handleOnFadeEnd);
			DI_EventCenter.addListener("OnLevelClear", handleOnLevelClear);
		}
		
		public void OnDisable()
		{
			DI_EventCenter<FadeType, string>.removeListener("OnFadeEnd", handleOnFadeEnd);
			DI_EventCenter.removeListener("OnLevelClear", handleOnLevelClear);
		}
		
		public void handleOnLevelClear()
		{
			isPlaying = false;
		}
		
		public void handleOnFadeEnd(FadeType fadeType, string fadeName)
		{
			if (fadeName == "Level Start") {
				if (fadeType == FadeType.IN) {
					isPlaying = true;
				}
			}
		}
		#endregion

		#region Input
		public void updateKeyCache()
		{
			jumpKeyIndex = DI_BindManager.getKeyIndex("Jump", playerId);
			//powerKeyIndex = DI_BindManager.getKeyIndex("Power", playerId);
		}
		#endregion
		
		#region Updates
		protected new void EarlyGlobalUpdate()
		{
			if (isPlaying) {
				if (!gravity.isInGravity && !isAiming) {
					// Rotate the player with their momentum.
					if (lastPosition != Vector3.zero) {
						transform.rotation = DI_ObjectHelper.lookAt2D(transform.position - lastPosition);
					}
					//Quaternion.LookRotation(gravity.rigidBody.velocity);
				}
				if (boostCooldownTimer > 0.0f) {
					boostCooldownTimer -= DI_Time.getTimeDelta();
				}
				
				lastPosition = transform.position;
			}
		}
		protected new void LateGlobalUpdate()
		{
		}
		
		public new void Update()
		{
			EarlyGlobalUpdate();
			base.Update();
			LateGlobalUpdate();
		}
		#endregion

		// State Definitions
		#region Idle
		protected void Idle_Enter(Enum previous)
		{
			DI_Debug.writeLog(DI_DebugLevel.INFO, "Enter " + "Idle");
		}
		
		protected void Idle_Update()
		{
			if (gravity.isInGravity) {
				ChangeCurrentState(PlayerState.Orbiting, false);
			}
			else {
				ChangeCurrentState(PlayerState.Flying, false);
			}
		}
		
		#endregion

		#region Flying
		protected void Flying_Enter(Enum previous)
		{
			DI_Debug.writeLog(DI_DebugLevel.INFO, "Enter " + "Flying");
		}
		
		protected void Flying_Update()
		{
			if (gravity.isInGravity) {
				ChangeCurrentState(PlayerState.Orbiting, false);
			}
		}
		
		#endregion

		#region Orbiting
		protected void Orbiting_Enter(Enum previous)
		{
			DI_Debug.writeLog(DI_DebugLevel.INFO, "Enter " + "Orbiting");
		}
		
		protected void Orbiting_Update()
		{
			if (!gravity.isInGravity) {
				ChangeCurrentState(PlayerState.Flying, false);
			}
			if (isAiming) {
				ChangeCurrentState(PlayerState.Aiming, false);
			}

			if (DI_BindManager.getKeyState(jumpKeyIndex, playerId) == DI_KeyState.AXIS_POSITIVE) {
				ChangeCurrentState(PlayerState.Aiming, false);
			}
		}
		
		#endregion

		#region Aiming
		protected void Aiming_Enter(Enum previous)
		{
			DI_Debug.writeLog(DI_DebugLevel.INFO, "Enter " + "Aiming");
			startSlow();
		}
		
		protected void Aiming_Update()
		{
			// We throw the player after they stop aiming.
			mouseCords = UnityEngine.Camera.main.ScreenToWorldPoint(Input.mousePosition);
			transform.rotation = DI_ObjectHelper.lookAt2D(mouseCords);
			pointer.transform.rotation = DI_ObjectHelper.lookAt2D(mouseCords, pointer.transform.position);

			if (DI_BindManager.getKeyState(jumpKeyIndex, playerId) != DI_KeyState.AXIS_POSITIVE) {
				gravity.exitGravity();
				ChangeCurrentState(PlayerState.Flying, false);
			}
		}

		protected void Aiming_Exit(Enum previous)
		{
			stopSlow();
		}
		
		#endregion

		#region Removed code, might be able to use again later.
//		if (isAiming) {
//			mouseCords = UnityEngine.Camera.main.ScreenToWorldPoint(Input.mousePosition);
//			transform.rotation = DI_ObjectHelper.lookAt2D(mouseCords);
//			pointer.transform.rotation = DI_ObjectHelper.lookAt2D(mouseCords, pointer.transform.position);
//			boostDirection = mouseCords;
//			Debug.DrawLine(transform.position, mouseCords, Color.green);
//		}
		
//		public void useBoost()
//		{
//			stopAiming();
//			transform.rotation = DI_ObjectHelper.lookAt2D(mouseCords);
//			gravity.rigidBody.isKinematic = true;
//			gravity.rigidBody.velocity = new Vector2(0f, 0f);
//			gravity.rigidBody.isKinematic = false;
//			//gravity.rigidBody.AddForce(new Vector2((transform.right.x * boostForce.x), (transform.right.y * boostForce.y)), ForceMode2D.Impulse);
//			gravity.rigidBody.AddForce((mouseCords - transform.transform.position) * boostForce.x, ForceMode2D.Impulse);
//			currentEnergy -= boostEnergyCost;
//			boostCooldownTimer = boostCooldown;
//		}

		#endregion

		#region Time Manipulation
		public void startSlow()
		{
			isSlowingTime = true;
			DI_Time.setTimeMultipler(slowEffect);
		}

		public void stopSlow()
		{
			isSlowingTime = false;
			DI_Time.setTimeMultipler(1f);
		}
		#endregion

		#region Handle Collisions
		public void OnCollisionEnter2D(Collision2D other)
		{
			//Reset the map if its a white blood cell or other hazard.
			if (other.transform.tag == "Obstacle") {
				Respawn();
			}
			else {
				//Play squish sound if its a wall.
				DI_SFX.playClipAtPoint(transform.position, impactSFX[UnityEngine.Random.Range(0, impactSFX.Count)].properties);

				// Regen 2 energy per bounce.
				currentEnergy = Mathf.Clamp(currentEnergy + 2f, 0, maxEnergy);
			}
		}

		public void Respawn()
		{
			gravity.rigidBody.isKinematic = true;
			gravity.rigidBody.velocity = new Vector2(0f, 0f);
			transform.position = spawnPoint.position;
			gravity.rigidBody.isKinematic = false;
			DI_EventCenter.invoke("OnRespawn");
			currentEnergy = maxEnergy / 2f;
		}
		#endregion
	}
}