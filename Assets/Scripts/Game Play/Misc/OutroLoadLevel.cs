﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2015
//
// TODO: Include a description of the file here.
//

using DI.Core.Behaviours;
using UnityEngine.SceneManagement;
using UnityEngine;
using System;

namespace DI.Misc
{
	public class OutroLoadLevel : DI_MonoBehaviour
	{
		public string levelName;
		public int trophyID = 50142;

		public void OnEnable()
		{
			try {
				GameJolt.API.Trophies.Unlock(trophyID, (bool success) => {
					if (success) {
						Debug.Log("Success!");
					}
					else {
						Debug.Log("Something went wrong");
					}
				});

				GameJolt.UI.Manager.Instance.QueueNotification("Thanks for playing our game, here is a bonus level we used for testing the mechanics.");
			}

			catch (Exception) {
			}

			SceneManager.LoadScene(levelName, LoadSceneMode.Single);
		}
	}
}