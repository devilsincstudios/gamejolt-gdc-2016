// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using DI.Core.Behaviours;

namespace DI.Misc
{
	public class FollowTarget : DI_MonoBehaviour
	{
		public GameObject objectToFollow;

		public void Update()
		{
			transform.position = objectToFollow.transform.position;
		}
	}
}