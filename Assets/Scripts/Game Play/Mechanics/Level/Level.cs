// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core.Behaviours;
using DI.Core.Events;
using DI.Mechanics.Gravity;
using DI.Core;
using DI.UI;

using UnityEngine.SceneManagement;
using UnityEngine;
using System.Collections.Generic;

namespace DI.Mechanics.Level
{
	public class Level : DI_MonoBehaviour
	{
		public List<Gravity2DBody> bloodCells;
		public string currentLevelName;
		public string nextLevel;
		public float levelStartTime;
		public float levelClearTime;
		public float deaths;
		public float score;

		public Dictionary<string, int> leaderBoardIds;

		public void OnEnable()
		{
			DI_EventCenter<string>.addListener("OnSetNextLevel", handleOnSetNextLevel);
			DI_EventCenter.addListener("OnLevelClear", handleOnLevelClear);
			DI_EventCenter.addListener("OnRespawn", handleOnRespawn);
			DI_EventCenter.addListener("OnChangeLevel", handleOnChangeLevel);
			DI_EventCenter<Gravity2DBody>.addListener("OnRegisterBloodCell", handleOnRegisterBloodCell);
			DI_EventCenter<FadeType, string>.addListener("OnFadeEnd", handleOnFadeEnd);

			// This data is taken from game jolt and may need to be updated if we change our leaderboards.
			leaderBoardIds = new Dictionary<string, int>();
			leaderBoardIds.Add("Level 1-1", 128732);
			leaderBoardIds.Add("Level 1-2", 128733);
			leaderBoardIds.Add("Level 1-3", 128734);
			leaderBoardIds.Add("Level 1-4", 128962);
			leaderBoardIds.Add("Level 1-5", 128963);
			leaderBoardIds.Add("Level 1-6", 128964);
			leaderBoardIds.Add("Level 1-7", 128965);
			leaderBoardIds.Add("Level 1-8", 128966);
			leaderBoardIds.Add("Level 1-9", 128967);
			leaderBoardIds.Add("Level 1-10", 128968);
			leaderBoardIds.Add("Dev Land", 128270);
		}

		public void OnDisable()
		{
			DI_EventCenter<string>.removeListener("OnSetNextLevel", handleOnSetNextLevel);
			DI_EventCenter.removeListener("OnLevelClear", handleOnLevelClear);
			DI_EventCenter.removeListener("OnRespawn", handleOnRespawn);
			DI_EventCenter.removeListener("OnChangeLevel", handleOnChangeLevel);
			DI_EventCenter<Gravity2DBody>.removeListener("OnRegisterBloodCell", handleOnRegisterBloodCell);
			DI_EventCenter<FadeType, string>.addListener("OnFadeEnd", handleOnFadeEnd);
		}

		public void handleOnFadeEnd(FadeType type, string fadeName)
		{
			if (fadeName == "Level Start") {
				levelStartTime = Time.timeSinceLevelLoad;
			}
			if (fadeName == "Level End") {
				showEndGameMenu();
			}
		}

		public void handleOnChangeLevel()
		{
			if (nextLevel != "") {
				SceneManager.LoadScene(nextLevel);
			}
		}

		public void showEndGameMenu()
		{
			score = (levelClearTime * 100 ) + (10f * getLivingBloodCellCount());
			DI_EventCenter<float, float, float, float>.invoke("OnShowEndGameMenu", score, getLivingBloodCellCount(), getTotalBloodCellCount(), deaths);

			//string scoreText = currentLevelName;
			int tableId = 0;
			string extraData = "";
			if (leaderBoardIds.TryGetValue(currentLevelName, out tableId)) {
				GameJolt.API.Scores.Add(Mathf.RoundToInt(score), Mathf.RoundToInt(score).ToString(), tableId, extraData, (bool success) => {
					Debug.Log(string.Format("Score Add {0}.", success ? "Successful" : "Failed"));
				});
			}
			else {
				Debug.Log("Unable to fetch the leaderboard table id for level: " + currentLevelName);
			}
		}

		public void onAdvanceLevel()
		{
			SceneManager.LoadScene(nextLevel, LoadSceneMode.Single);
		}

		public void handleOnSetNextLevel(string level)
		{
			nextLevel = level;
		}

		public void handleOnLevelClear()
		{
			levelClearTime = DI_Time.getTimeSinceLevelLoad() - levelStartTime;
			DI_EventCenter<string>.invoke("OnStartFade", "Level End");
		}

		public void handleOnRespawn()
		{
			deaths += 1;
			levelStartTime = DI_Time.getTimeSinceLevelLoad();
		}

		public void handleOnRegisterBloodCell(Gravity2DBody bloodCell)
		{
			bloodCells.Add(bloodCell);
		}

		public int getTotalBloodCellCount()
		{
			return bloodCells.Count;
		}

		public int getLivingBloodCellCount()
		{
			int livingCells = 0;

			for (int index = 0; index < bloodCells.Count; index++) {
				if (!bloodCells[index].isDead) {
					livingCells += 1;
				}
			}

			return livingCells;
		}

		public int getDeadBloodCellCount()
		{
			return getTotalBloodCellCount() - getLivingBloodCellCount();
		}
	}
}