// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using DI.Core.Behaviours;
using DI.Core.Events;
using DI.Core;
using DI.Core.Helpers;

namespace DI.Mechanics.Gravity
{
	public class Gravity2D : DI_MonoBehaviour
	{
		public bool isInGravity = false;
		public GameObject gravitySource;
		public Rigidbody2D rigidBody;
		public float speed = 0.0f;
		public float force = 0.0f;

		public void Awake()
		{
			rigidBody = GetComponent<Rigidbody2D>();
		}

		public void OnEnable()
		{
			DI.Core.Events.DI_EventCenter<GameObject, GameObject, float, float>.addListener("OnGravityEnter", handleOnGravityEnter);
		}

		public void OnDisable()
		{
			DI.Core.Events.DI_EventCenter<GameObject, GameObject, float, float>.removeListener("OnGravityEnter", handleOnGravityEnter);
		}

		public void handleOnGravityEnter(GameObject objectEntering, GameObject objectSupplying, float rotationSpeed, float jumpForce)
		{
			if (objectEntering == this.gameObject) {
				gravitySource = objectSupplying;
				rigidBody.isKinematic = true;
				speed = rotationSpeed;
				force = jumpForce;
				isInGravity = true;
			}
		}

		public void exitGravity()
		{
			isInGravity = false;
			Vector3 mouseCords = UnityEngine.Camera.main.ScreenToWorldPoint(Input.mousePosition);
			transform.rotation = DI_ObjectHelper.lookAt2D(mouseCords);
			rigidBody.isKinematic = true;
			rigidBody.velocity = new Vector2(0f, 0f);
			rigidBody.isKinematic = false;
			rigidBody.AddForce((mouseCords - transform.transform.position) * force, ForceMode2D.Impulse);
			DI_EventCenter<GameObject>.invoke("OnGravityExit", gameObject);
		}

		public void Update()
		{
			if (isInGravity) {
				this.transform.RotateAround(gravitySource.transform.position, Vector3.forward, speed * DI_Time.getTimeDelta());
				transform.rotation = DI_ObjectHelper.lookAt2D(gravitySource.transform, transform);
			}
		}
	}
}