// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using DI.Core.Behaviours;
using DI.Core.Events;
using DI.Core;

namespace DI.Mechanics.Gravity
{
	public class Gravity2DForce : DI_MonoBehaviour
	{
		public Vector2 forceApplied;
		public Vector2 forcePoint;
		public Rigidbody2D lastRigidBody;
		public GameObject lastObject;

		public Animator animator;
		public float transitionTime = 2.0f;
		public float transitionTimer = 0.0f;

		public ParticleSystem particles;

		// Starts at base state
		public int state = 0;

		public void OnEnable()
		{
			Vector3 localPoint = transform.TransformPoint(Vector3.zero);
			forcePoint = new Vector2(localPoint.x, localPoint.y);
			// Start at a random time.
			transitionTimer += UnityEngine.Random.Range(0f, transitionTime);
		}

		public void Update()
		{
			if (transitionTimer > transitionTime) {
				transitionTimer = 0.0f;
				switch (state) {
					case 0:
						animator.SetTrigger("Prep");
						state = 1;
						break;
					case 1:
						animator.SetTrigger("Explode");
						transitionTime += 2.5f;
						state = 2;
						break;
					case 2:
						transitionTime -= 2.5f;
						animator.SetTrigger("Reset");
						state = 0;
						break;
				}
			}

			transitionTimer += DI_Time.getTimeDelta();
		}

		public void OnTriggerEnter2D(Collider2D other)
		{
			if (other.tag != "Wall") {
				lastObject = other.gameObject;
				lastRigidBody = other.GetComponent<Rigidbody2D>();
			}
		}

		public void OnTriggerStay2D(Collider2D other)
		{
			if (other.tag != "Wall") {
				if (other.gameObject == lastObject) {
					if (particles.gameObject.activeSelf) {
						lastRigidBody.AddForceAtPosition(forceApplied, forcePoint, ForceMode2D.Impulse);
					}
				}
			}
		}
	}
}