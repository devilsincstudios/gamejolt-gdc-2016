﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using DI.Core.Behaviours;
using DI.Core.Events;
using DI.Core;
using DI.SFX;

// TODO
// Split this into the generic 2d Body class and a child blood cell only class.
//

namespace DI.Mechanics.Gravity
{
	[RequireComponent(typeof(Rigidbody2D))]
	public class Gravity2DBody : DI_MonoBehaviour
	{
		[Header("General Body Settings")]
		public float orbitSpeed = 2.0f;
		public float jumpForce = 2.0f;
		private GameObject orbiter;
		private float delayTimer = 0.0f;

		[Header("Blood Cell Settings")]
		public SpriteRenderer spriteRenderer;
		public CircleCollider2D bodyCollider;
		public bool isDead = false;
		public bool isBloodCell = false;
		public float maxHealth = 100f;
		public float currentHealth = 100f;
		public float healthDamageTime = 0.1f;
		public float healthTimer = 0.0f;
		public float damageAmount = 25f;
		public DI_SFXClip damageSound;
		public List<DI_SFXClip> deathSounds;
		public Animator animator;

		[Header("Exit Node Settings")]
		public bool isExitNode = false;

		public void OnEnable()
		{
			DI_EventCenter<GameObject>.addListener("OnGravityExit", handleOnGravityExit);
			DI_EventCenter.addListener("OnRespawn", handleOnRespawn);

			if (isBloodCell) {
				DI_EventCenter<Gravity2DBody>.invoke("OnRegisterBloodCell", this);
			}
		}
		
		public void OnDisable()
		{
			DI_EventCenter<GameObject>.removeListener("OnGravityExit", handleOnGravityExit);
			DI_EventCenter.removeListener("OnRespawn", handleOnRespawn);
		}

		public void handleOnGravityExit(GameObject objectExiting)
		{
			if (objectExiting == orbiter) {
				delayTimer = 0.25f;
				// Cell is dead.
				if (isDead) {
					spriteRenderer.enabled = false;
					DI_SFX.playClipAtPoint(transform.position, deathSounds[UnityEngine.Random.Range(0, deathSounds.Count)].properties);
					bodyCollider.enabled = false;
				}
			}
		}

		public void handleOnRespawn()
		{
			delayTimer = 0.0f;
			orbiter = null;
			if (isBloodCell) {
				currentHealth = maxHealth;
				animator.SetFloat("Health", currentHealth);
				isDead = false;
				spriteRenderer.enabled = true;
				bodyCollider.enabled = true;
			}
		}

		public void Update()
		{
			if (delayTimer > 0.0f) {
				delayTimer -= DI_Time.getTimeDelta();
				if (delayTimer <= 0.0f) {
					orbiter = null;
				}
			}
			if (isBloodCell) {
				if (healthTimer > 0.0f) {
					healthTimer -= DI_Time.getTimeDelta();
				}
			}
		}

		public void OnTriggerEnter2D (Collider2D other)
		{
			if (other.tag == "Player") {
				if (!isDead) {
					// Don't allow the same object to get sucked back into oribt.
					if (delayTimer <= 0.0f) {
						if (orbiter != other.gameObject) {
							DI.Core.Events.DI_EventCenter<GameObject, GameObject, float, float>.invoke("OnGravityEnter", other.gameObject, this.gameObject, orbitSpeed, jumpForce);
						}
						orbiter = other.gameObject;
						if (isExitNode) {
							DI_EventCenter.invoke("OnLevelClear");
						}
					}
				}
			}
		}

		public void OnTriggerStay2D (Collider2D other) {
			if (other.tag == "Player") {
				if (!isDead) {
					if (isBloodCell) {
						if (currentHealth > 0f) {
							if (healthTimer <= 0.0f) {
								currentHealth -= damageAmount;
								healthTimer = healthDamageTime;
								animator.SetFloat("Health", currentHealth);
								DI_SFX.playClipAtPoint(transform.position, damageSound.properties);
							}
						}
						else {
							isDead = true;
						}
					}
				}
			}
		}
	}
}