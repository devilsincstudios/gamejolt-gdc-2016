﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEditor;
using System;
using DI.Core.Debug;
using UnityEngine;

namespace DI.Core.Input
{
	public static class Input_Bindings
	{
		[MenuItem("Devil's Inc Studios/Input/Save Bindings")]
		public static void save()
		{
			DI_BindManager.bindings = new DI_Bindings();
			DI_BindManager.bindings.boundKeys = new System.Collections.Generic.List<System.Collections.Generic.List<DI_KeyBind>>();
			DI_BindManager.bindings.boundKeys.Add(InputManager.instance.bindingsPlayerOne);
			DI_BindManager.bindings.boundKeys.Add(InputManager.instance.bindingsPlayerTwo);
			DI_BindManager.saveBoundKeys();
		}
		[MenuItem("Devil's Inc Studios/Input/Load Bindings")]
		public static void load()
		{
			DI_BindManager.loadBoundKeys();
			InputManager manager = GameObject.Find("Management").GetComponent<InputManager>();
			manager.bindingsPlayerOne = DI_BindManager.bindings.boundKeys[0];
			manager.bindingsPlayerTwo = DI_BindManager.bindings.boundKeys[1];
		}
	}
}