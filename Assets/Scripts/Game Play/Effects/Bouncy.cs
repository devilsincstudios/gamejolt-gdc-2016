// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;
using DI.Core.Behaviours;
using DI.Core;

namespace DI.Effects
{
	public class Bouncy : DI_MonoBehaviour
	{
		public float bounceHeight = 0.0f;
		public float bounceTime = 2.0f;
		public bool isUpBounce = false;

		private float startTimer = 0.0f;

		public void OnEnable()
		{
			if (UnityEngine.Random.Range(0, 2) >= 1f) {
				isUpBounce = true;
			}
			else {
				isUpBounce = false;
			}

			startTimer = UnityEngine.Random.Range(0f, 0.25f);
		}

		public void Update()
		{
			if (startTimer <= 0.0f) {
				if (isUpBounce) {
					float yValue = Mathf.Lerp(transform.localPosition.y, transform.localPosition.y + bounceHeight, bounceTime);
					transform.localPosition = new Vector3(transform.localPosition.x, yValue, transform.localPosition.z);
					if (yValue >= ((transform.localPosition.y + bounceHeight) * 0.90f)) {
						isUpBounce = false;
					}
				}
				else {
					float yValue = Mathf.Lerp(transform.localPosition.y, transform.localPosition.y - bounceHeight, bounceTime);
					transform.localPosition = new Vector3(transform.localPosition.x, yValue, transform.localPosition.z);
					if (yValue <= ((transform.localPosition.y - bounceHeight) * 0.90f)) {
						isUpBounce = true;
					}
				}
			}
			else {
				startTimer -= DI_Time.getTimeDelta();
			}
		}
	}
}