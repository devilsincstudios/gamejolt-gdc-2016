// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using UnityEngine;

using DI.Core.Behaviours;
using DI.Core.Debug;
using DI.Core.Events;

namespace DI.Core.Triggers
{
	[AddComponentMenu("Devil's Inc Studios/Triggers/TriggerDetector")]
	public class TriggerDetector : DI_MonoBehaviour
	{
		public void OnTriggerEnter(Collider other)
		{
			DI_EventCenter<Collider>.invoke("OnTriggerEnter", other);
			DI_Debug.writeLog(DI_DebugLevel.INFO, "OnTriggerEnter: " + other.gameObject.name);
		}

		public void OnTriggerExit(Collider other)
		{
			DI_EventCenter<Collider>.invoke("OnTriggerExit", other);
			DI_Debug.writeLog(DI_DebugLevel.INFO, "OnTriggerExit: " + other.gameObject.name);
		}

		public void OnTriggerStay(Collider other)
		{
			DI_EventCenter<Collider>.invoke("OnTriggerStay", other);
			DI_Debug.writeLog(DI_DebugLevel.INFO, "OnTriggerStay: " + other.gameObject.name);
		}
	}
}