﻿/// <summary>
/// Script to control the change of textures on an object
/// Uses a public list to set the textures and display them
/// </summary>

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DisplayTexture : MonoBehaviour {

    public List<Texture2D> Texture = new List<Texture2D>();
    public float Delay;
    public float minWait;
    public float maxWait;
    private int _textureCounter = 0;


    void Start() {
        Invoke("CycleTextures", Delay);
    }


    void CycleTextures() {
        // Causes the cycle time to now be random
        Delay = Random.Range(minWait, maxWait);

        // Will check the amount of images and then render them in sequence
        _textureCounter = ++_textureCounter % Texture.Count;

        // Render the texture selected
        GetComponent<Renderer>().material.mainTexture = Texture[_textureCounter];

        // Resets the program
        Invoke("CycleTextures", Delay);
    }
}