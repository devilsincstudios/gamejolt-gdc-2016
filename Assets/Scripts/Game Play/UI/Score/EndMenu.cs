﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core.Behaviours;
using DI.Core.Events;
using DI.Core;
using DI.UI;

using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

namespace DI.UI.Score
{
	public class EndMenu : DI_MonoBehaviour
	{
		public GameObject endMenuCanvas;
		public Text scoreText;
		public Text livingCellText;
		public Text totalCellText;
		public Text deathText;

		public void OnEnable()
		{
			DI_EventCenter<float, float, float, float>.addListener("OnShowEndGameMenu", handleOnShowEndGameMenu);
			// DI_EventCenter<float, float, float, float>.invoke("OnShowEndGameMenu", score, getLivingBloodCellCount(), getTotalBloodCellCount(), deaths);
		}
		public void OnDisable()
		{
			DI_EventCenter<float, float, float, float>.removeListener("OnShowEndGameMenu", handleOnShowEndGameMenu);
		}

		public void handleOnShowEndGameMenu(float score, float livingCells, float totalCells, float deaths)
		{
			endMenuCanvas.gameObject.SetActive(true);
			scoreText.text = score.ToString();
			livingCellText.text = livingCells.ToString();
			totalCellText.text = totalCells.ToString();
			deathText.text = deaths.ToString();
		}

		public void closeFrame()
		{
			endMenuCanvas.gameObject.SetActive(false);
			DI_EventCenter.invoke("OnChangeLevel");
		}
	}
}