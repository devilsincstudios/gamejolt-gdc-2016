// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core.Behaviours;
using DI.Core;
using DI.Mechanics.Level;

using UnityEngine.UI;
using UnityEngine;

namespace DI.UI.Score
{
	public class Time : DI_MonoBehaviour
	{
		public Text uiTimer;
		public Level level;

		public void Update()
		{
			uiTimer.text = (Mathf.Round((DI_Time.getTimeSinceLevelLoad() - level.levelStartTime) * 100f) / 100f).ToString();
		}
	}
}