// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using DI.Core.Behaviours;
using DI.Core;
using DI.Mechanics.Level;

using UnityEngine.UI;
using UnityEngine;

namespace DI.UI.Score
{
	public class BloodCells : DI_MonoBehaviour
	{
		public Text deadCells;
		public Text livingCells;
		public Level level;

		public void Update()
		{
			deadCells.text = level.getDeadBloodCellCount().ToString();
			livingCells.text = level.getLivingBloodCellCount().ToString();
		}
	}
}