﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2015
//
// TODO: Include a description of the file here.
//

using DI.Core.Behaviours;

using UnityEngine.SceneManagement;
using UnityEngine;

namespace DI.UI.Menu
{
	public class Menu : DI_MonoBehaviour
	{
		public string firstLevel;
		public GameObject creditsMenu;
		public GameObject mainMenu;
		public void playGame()
		{
			SceneManager.LoadScene(firstLevel);
		}

		public void showOptions()
		{
		}

		public void showCredits()
		{
			creditsMenu.SetActive(true);
			mainMenu.SetActive(false);
		}

		public void closeCredits()
		{
			creditsMenu.SetActive(false);
			mainMenu.SetActive(true);
		}

		public void exitGame()
		{
			Application.Quit();
		}
	}
}