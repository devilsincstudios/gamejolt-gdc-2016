﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//
using UnityEngine;
using UnityEngine.SceneManagement;

using DI.Core.Behaviours;

namespace DI.UI
{
	public class DevLevelSelect : DI_MonoBehaviour
	{
		public int levelId = 0;

		public void setLevelId(int newLevelId)
		{
			levelId = newLevelId + 1;
		}

		public void changeLevel()
		{
			SceneManager.LoadScene(levelId);
		}
	}
}