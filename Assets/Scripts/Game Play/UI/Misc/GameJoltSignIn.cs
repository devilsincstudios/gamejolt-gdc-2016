﻿// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2015
//
// TODO: Include a description of the file here.
//


using DI.Core.Behaviours;

namespace DI.UI.Misc
{
	public class GameJoltSignIn : DI_MonoBehaviour
	{

		public void OnEnable()
		{
			bool isSignedIn = (GameJolt.API.Manager.Instance.CurrentUser != null);

			if (!isSignedIn) {
				GameJolt.UI.Manager.Instance.ShowSignIn();
			}
		}
	}
}
