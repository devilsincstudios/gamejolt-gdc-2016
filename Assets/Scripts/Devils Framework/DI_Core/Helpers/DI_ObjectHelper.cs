// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.

using UnityEngine;

namespace DI.Core.Helpers
{
	public static class DI_ObjectHelper
	{
		public static GameObject createGameObject(string name)
		{
			GameObject go = new GameObject(name);
			return go;
		}
		
		public static GameObject createGameObject(string name, GameObject parent)
		{
			GameObject go = createGameObject(name);
			go.transform.parent = parent.transform;
			return go;
		}

		public static Vector3 getBackOfObject(Transform position)
		{
			return position.transform.position - position.forward;
		}

		public static Vector3 getBackOfObject(Transform position, float distanceBehind)
		{
			return position.transform.position - (position.forward * distanceBehind);
		}

		public static Vector2 getBackOfObject2D(Transform position)
		{
			Vector3 location = getBackOfObject(position);
			return new Vector2(location.x, location.y);
		}
		
		public static Vector2 getBackOfObject2D(Transform position, float distanceBehind)
		{
			Vector3 location = getBackOfObject(position, distanceBehind);
			return new Vector2(location.x, location.y);
		}

		public static Vector3 getRightOfObject(Transform position)
		{
			return position.transform.position + position.right;
		}

		public static Vector2 getRightOfObject2D(Transform position)
		{
			Vector3 location = getRightOfObject(position);
			return new Vector2(location.x, location.y);
		}

		public static Vector3 getLeftOfObject(Transform position)
		{
			return position.transform.position - position.right;
		}

		public static Vector2 getLeftOfObject2D(Transform position)
		{
			Vector3 location = getLeftOfObject(position);
			return new Vector2(location.x, location.y);
		}

		public static Quaternion lookAt2D(Transform target, Transform transform)
		{
			Vector3 direction = target.position - transform.position;
			float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
			return Quaternion.AngleAxis(angle, Vector3.forward);
		}

		public static Quaternion lookAt2D(Vector3 target, Vector3 transform)
		{
			Vector3 direction = target - transform;
			float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
			return Quaternion.AngleAxis(angle, Vector3.forward);
		}

		public static Quaternion lookAt2D(Vector2 target)
		{
			float angle = Mathf.Atan2(target.x,target.y);
			return Quaternion.Euler(0f,0f,angle*Mathf.Rad2Deg);
		}
	}
}