// Devils Inc Studios
// Copyright DEVILS INC. STUDIOS LIMITED 2016
//
// TODO: Include a description of the file here.
//

using System.Collections.Generic;
using System.Xml.Serialization;
using System.Xml;
using System;
using System.IO;
using System.Text;

using UnityEngine;
using DI.Core.Debug;

namespace DI.Core.Input
{
	public static class DI_BindManager
	{
		[XmlElement("Bindings")]
		public static DI_Bindings bindings;
		public static string configFile;

		public static void loadBoundKeys()
		{
			// Path is relative to the resources folder.
			configFile = "Config/keybindings";

			XmlSerializer serializer = new XmlSerializer(typeof(DI_Bindings));
			string configContents = Resources.Load<TextAsset>(configFile).text;
			MemoryStream memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(configContents));
			bindings = serializer.Deserialize(memoryStream) as DI_Bindings;
			memoryStream.Close();
		}

		// Only save the keys if this is a standalone build
		public static void saveBoundKeys()
		{
			#if UNITY_STANDALONE
			// Path is realtive to the project folder
			configFile = "Assets/Resources/Config/keybindings.xml";

			XmlSerializer serializer = new XmlSerializer(typeof(DI_Bindings));
			Encoding encoding = Encoding.GetEncoding("UTF-8");

			using(MemoryStream memoryStream = new MemoryStream())
			{
				serializer.Serialize(memoryStream, bindings);
				UnityEngine.Debug.Log(encoding.GetString(memoryStream.ToArray()));
				memoryStream.WriteTo(new FileStream(configFile, FileMode.Truncate, FileAccess.Write));
				UnityEngine.Debug.Log("Saving config");
				printBinds();
			}
			#endif
		}

		public static void printBinds()
		{
			DI_Debug.writeLog(DI_DebugLevel.CRITICAL, "BindManager: printBinds()");
			for (int playerId = 0; playerId < bindings.boundKeys.Count; playerId++) {
				DI_Debug.writeLog(DI_DebugLevel.CRITICAL, "BindManager: playerId: " + playerId);
				for (int player = 0; player < bindings.boundKeys.Count; player++) {
					for (int iteration = 0; iteration < bindings.boundKeys.Count; iteration++) {
						DI_Debug.writeLog(DI_DebugLevel.CRITICAL, bindings.boundKeys[player][iteration].toString());
					}
				}
			}
			for (int playerId = 0; playerId < bindings.controllerTypes.Count; playerId++) {
				DI_Debug.writeLog(DI_DebugLevel.CRITICAL, "BindManager: Controller Type playerId: " + playerId + " = " + getControllerType(playerId).ToString());
			}
		}

		public static void setKeyState(int bindIndex, int playerId, DI_KeyState state)
		{
			bindings.boundKeys[playerId][bindIndex].bindState = state;
		}

		public static DI_KeyState getKeyState(int bindIndex, int playerId)
		{
			if (bindIndex != -1) {
				return bindings.boundKeys[playerId][bindIndex].bindState;
			}
			else {
				UnityEngine.Debug.LogException(new Exception("Unable to getKeyState on an unbound key."));
				return DI.Core.Input.DI_KeyState.UNKNOWN;
			}
		}

		public static void setControllerType(DI_ControllerType type, int playerId)
		{
			if (bindings.controllerTypes.Count > playerId) {
				bindings.controllerTypes[playerId] = type;
			}
			else {
				while (bindings.controllerTypes.Count < playerId) {
					bindings.controllerTypes.Add(DI_ControllerType.NONE);
				}
				bindings.controllerTypes.Add(type);
			}
		}
		public static DI_ControllerType getControllerType(int playerId)
		{
			if (bindings.controllerTypes.Count > playerId) {
				return bindings.controllerTypes[playerId];
			}
			DI_Debug.writeLog(DI_DebugLevel.HIGH, "getControllerType(" + playerId + ") was called but playerId did not exist in the controllerTypes list.");
			return DI_ControllerType.NONE;
		}

		public static int getKeyIndex(string bindName, int playerId)
		{
			if (bindings == null) {
				DI_Debug.writeLog(DI_DebugLevel.HIGH, "Bound keys has not been set yet, but access was requested.");
				bindings = new DI_Bindings();
			}

			if (bindings.boundKeys != null) {
				if (bindings.boundKeys.Count > playerId) {
					for (int iteration = 0; iteration < bindings.boundKeys[playerId].Count; iteration++) {
						if (bindings.boundKeys[playerId][iteration].bindName == bindName) {
							DI_Debug.writeLog(DI_DebugLevel.INFO, "GetKeyIndex (" + bindName + ":" + playerId + ") returned: " + iteration);
							return iteration;
						}
					}
				}
			}

			UnityEngine.Debug.LogException(new Exception("Bound key: " + bindName + " requested for player: " + playerId + " but that key is not bound to that player."));
			return -1;
		}

		public static void setKey(DI_KeyBind keyBinding)
		{
			if (bindings == null) {
				DI_Debug.writeLog(DI_DebugLevel.HIGH, "Bound keys has not been set yet, but access was requested.");
				bindings = new DI_Bindings();
			}
			if (keyBinding.playerId > bindings.boundKeys.Count - 1) {
				DI_Debug.writeLog(DI_DebugLevel.INFO, "Attempted to add a key to a non-existant player - populating new player(s).");
				while (keyBinding.playerId >= bindings.boundKeys.Count) {
					DI_Debug.writeLog(DI_DebugLevel.INFO, "Adding player: " + bindings.boundKeys.Count + " to key bindings.");
					bindings.boundKeys.Add(new List<DI_KeyBind>());
				}
			}

			if (bindings.boundKeys.Count >= keyBinding.playerId) {
				int keyIndex = getKeyIndex(keyBinding.bindName, keyBinding.playerId);

				DI_Debug.writeLog(DI_DebugLevel.INFO, "Bind Manager: " + keyBinding.toString());
				if (keyIndex != -1) {
					bindings.boundKeys[keyBinding.playerId][keyIndex] = keyBinding;
				}
				else {
					bindings.boundKeys[keyBinding.playerId].Add(keyBinding);
				}
			}
		}

		public static string getKey(string bindName, int playerId)
		{
			if (bindings == null) {
				DI_Debug.writeLog(DI_DebugLevel.HIGH, "Bound keys has not been set yet, but access was requested.");
				bindings = new DI_Bindings();
			}

			if (bindings.boundKeys.Count >= playerId) {
				for (int iteration = 0; iteration < bindings.boundKeys.Count; iteration++) {
					if (bindings.boundKeys[playerId][iteration].bindName == bindName) {
						return bindings.boundKeys[playerId][iteration].bindKey;
					}
				}
			}
			else {
				DI_Debug.writeLog(DI_DebugLevel.MEDIUM, "Attempting to get a key from a non-registered player id.");
			}
			return null;
		}

		public static float getAxisValue(int keyIndex, int playerId)
		{
			return UnityEngine.Input.GetAxis(bindings.boundKeys[playerId][keyIndex].bindKey);
		}

		public static DI_KeyBindType getBindType(string bindName, int playerId)
		{
			if (bindings == null) {
				DI_Debug.writeLog(DI_DebugLevel.HIGH, "Bound keys has not been set yet, but access was requested.");
				bindings = new DI_Bindings();
			}

			if (bindings.boundKeys.Count >= playerId) {
				for (int iteration = 0; iteration < bindings.boundKeys.Count; iteration++) {
					if (bindings.boundKeys[playerId][iteration].bindName == bindName) {
						return bindings.boundKeys[playerId][iteration].bindType;
					}
				}
			}
			return DI_KeyBindType.UNDEFINED;
		}


		public static void setBindType(string bindName, int playerId, DI_KeyBindType type)
		{
			if (bindings == null) {
				DI_Debug.writeLog(DI_DebugLevel.HIGH, "Bound keys has not been set yet, but access was requested.");
				bindings = new DI_Bindings();
			}

			int keyIndex = -1;

			if (bindings.boundKeys.Count >= playerId) {
				for (int iteration = 0; iteration < bindings.boundKeys.Count; iteration++) {
					if (bindings.boundKeys[playerId][iteration].bindName == bindName) {
						keyIndex = iteration;
					}
				}
			}

			if (keyIndex != -1) {
				DI_KeyBind bind = bindings.boundKeys[playerId][keyIndex];
				bind.bindType = type;
				bindings.boundKeys[playerId][keyIndex] = bind;
			}
		}
	}
}

